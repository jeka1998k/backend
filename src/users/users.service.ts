import {HttpException, Injectable} from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import {UpdateUserDto} from "./dto/update-user.dto";
import {InjectModel} from "@nestjs/mongoose";
import {User} from "./schemas/user.schema";
import {Model} from "mongoose";

@Injectable()
export class UsersService {
    constructor(@InjectModel(User.name) private userModel: Model<any>) {
    }

    async create(createUserDto: CreateUserDto) {
        const createdUser = new this.userModel(createUserDto);
        const {email} = createdUser
        const userExists = await this.userModel.findOne({email})
        if (userExists) {
            throw new HttpException('User already exists', 400)
        }
        const newUser = await createdUser.save()

        if (newUser) {
            return {
                id: newUser._id,
                name: newUser.name,
                email: newUser.email,
                isAdmin: newUser.isAdmin,
                isTeacher: newUser.isTeacher
            }
        } else {
            throw new HttpException('Invalid user data', 400)
        }
    }
    
    async findByCond(cond: any ) {

        const user  = await this.userModel.findOne(cond)
        return user
    }

    findById(_id: number) {
        return this.userModel.findOne({ _id })
    }

    findAll() {
        return `This action returns all users`;
    }

    findOne(id: number) {
        return `This action returns a #${id} user`;
    }

    update(id: number, updateUserDto: UpdateUserDto) {
        return `This action updates a #${id} user`;
    }

    remove(id: number) {
        return `This action removes a #${id} user`;
    }
}
