import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as bcrypt from 'bcrypt';

export type UserDocument = User & Document;

@Schema({
    timestamps: true
})
export class User {
    @Prop({required: true})
    name: string;

    @Prop({required: true, unique: true})
    email: string;

    @Prop({required: true})
    password: string;

    @Prop()
    phone: string;

    @Prop({required: true, default: false})
    isAdmin: boolean;

    @Prop({required: true, default: false})
    isTeacher: boolean;
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.methods.comparePassword = async function (password: string) {
    return bcrypt.compareSync(password, this.password);
}

export { UserSchema };
