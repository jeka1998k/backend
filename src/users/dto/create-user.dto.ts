import { IsEmail, Length } from "class-validator";

export class CreateUserDto {
    @Length(3,32,{message:'Минимум 3'})
    name: string

    @IsEmail(undefined,{message:'Неверная почта'})
    email:string

    @Length(6,32,{message:'Минимум 6 символов'})
    password: string

    phone?: string

    isAdmin: boolean

    isTeacher: boolean
}