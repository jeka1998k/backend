import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AllExceptionsFilter } from "./filter";
import {ValidationPipe} from "@nestjs/common";

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule, {
      logger: ['error', 'warn'],
    });
    app.useGlobalFilters(new AllExceptionsFilter());
    app.useGlobalPipes(new ValidationPipe());
    await app.listen(5000);

    console.log('Listen on port 5000')
  } catch (error) {
    throw new Error(`Error ${error.message}`)
  }
}
bootstrap();
