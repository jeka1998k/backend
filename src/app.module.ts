import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ConfigModule} from '@nestjs/config';
import { LanguagesModule } from './languages/languages.module';
import { UsersModule } from './users/users.module';
import {User, UserDocument, UserSchema} from "./users/schemas/user.schema";
import * as bcrypt from 'bcrypt';
import { AuthModule } from "./auth/auth.module";

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true
        }),
        MongooseModule.forRoot(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }),
        MongooseModule.forFeatureAsync([
            {
                name: User.name,
                useFactory: () => {
                    const schema = UserSchema;
                    schema.pre<UserDocument>('save', async function (next) {
                        if (!this.isModified('password')) return next();

                        const salt = await bcrypt.genSalt(10)
                        this.password = await bcrypt.hash(this.password, salt)
                    })
                    return schema;
                },
            },
        ]),
        LanguagesModule,
        UsersModule,
        AuthModule
    ],
})
export class AppModule {}
