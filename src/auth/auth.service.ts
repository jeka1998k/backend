import { ForbiddenException, HttpException, Injectable } from "@nestjs/common";
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from "../users/dto/create-user.dto";
import { UsersService } from "../users/users.service";
import { LoginUserDto } from "../users/dto/login-user.dto";

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findByCond({ email });

    if (user && await user.comparePassword(password)) {
      return user;
    }
    return null;
  }

  async login(loginUserDto: LoginUserDto) {
    const { email, password } = loginUserDto
    const user = await this.usersService.findByCond({ email })

    const payload = { email, sub: password };

    if (user && user.comparePassword(password)) {
      return {
        id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        isTeacher: user.isTeacher,
        token: this.jwtService.sign(payload),
      }
    } else {
      throw new HttpException('Invalid email or password', 401)
    }
  }

  async register(dto: CreateUserDto) {
      const user = await this.usersService.create(dto)
      const payload = { email: user.email, name: user.name };

      return {
        ...user,
        token: this.jwtService.sign(payload),
      }
    }
}
