import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { UsersService } from "../../users/users.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usersService : UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'superKey',
    });
  }

  async validate(payload: {sub : number, email : string }) {
    const user = await this.usersService.findById(payload.sub)
    if (!user){
      throw new UnauthorizedException('У вас нет доступа');
    }
    return { id: user._id, email: user.email  };
  }
}