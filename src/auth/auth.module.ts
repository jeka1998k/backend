import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from "./stratrgies/local.strategy";
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from "./stratrgies/jwt.strategy";
import { UsersModule } from "../users/users.module";

@Module({
  imports: [UsersModule, PassportModule,    JwtModule.register({
    secret: 'superKey',
    signOptions: { expiresIn: '30d' },
  }),],
  controllers: [AuthController],
  providers: [AuthService,LocalStrategy,JwtStrategy]
})
export class AuthModule {}
